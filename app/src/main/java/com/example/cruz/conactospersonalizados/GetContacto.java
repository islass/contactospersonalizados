package com.example.cruz.conactospersonalizados;

import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


/**
 * Created by Laura on 20/02/2019.
 */

public class GetContacto extends AsyncTask<Void,Void,Void> {

    public  String url;
    public  ArrayList<Contactos> contactos ;
    public ArrayList<String> contactosString;
    public ListaPersonalizada adapter ;
    public ArrayAdapter adaptador;
    public ImageView imagen;

    public GetContacto(String url, ArrayList<Contactos> contacto, ListaPersonalizada adapter, ImageView foto) {
        this.url = url;
        this.contactos= contacto;
        this.adapter= adapter;
        this.imagen = foto;
    }

    public GetContacto(String url, ArrayList<String> contactos, ArrayAdapter adapter) {
        this.url = url;
        this.contactosString = contactos;
        this.adaptador = adapter;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {

            URL uri = new URL(url);
            HttpURLConnection conexion = (HttpURLConnection) uri.openConnection();
            conexion.setRequestMethod("GET");
            InputStream entradaDatos = conexion.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(entradaDatos));
            String linea="";
            StringBuilder respuesta = new StringBuilder();
            while ((linea = reader.readLine()) != null)
            respuesta.append(linea);

            JSONArray contacto = new JSONArray(respuesta.toString());

            ArrayList<Contactos> contactos = new ArrayList<>();

            //ArrayList<String> contactosStrings = new ArrayList<>();
            for (int g = 0; g < contacto.length(); g++){

                Contactos personaTemp= new Contactos();

                String contactoTemp;
                JSONObject objetoContacto = contacto.getJSONObject(g);
                JSONArray telefonos= new JSONArray(objetoContacto.getString("telefonos"));
                contactoTemp=objetoContacto.getString("nombre");

                String fotobase64 = objetoContacto.getString("Foto");
                personaTemp.nomnbre= objetoContacto.getString("nombre");
                personaTemp.Foto= objetoContacto.getString("Foto");
                personaTemp.Telefono="";


                for (int i=0;i<telefonos.length();i++){
                    JSONObject objetoTelefonos = telefonos.getJSONObject(i);
                    //contactoTemp+="\n Telefono["+i+"]:" + objetoTelefonos.getString("numero");

                    personaTemp.Telefono="\n Telefono["+i+"]" + objetoTelefonos.get("numero");
                }
                contactos.add(personaTemp);
                //contactosStrings.add(contactoTemp);
            }
           // this.contactos.clear();
            this.contactos.addAll(contactos);
            //this.contactosString.addAll(contactosStrings);


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    return  null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        adapter.notifyDataSetChanged();
        //adaptador.notifyDataSetChanged();
        super.onPostExecute(aVoid);
    }
}
