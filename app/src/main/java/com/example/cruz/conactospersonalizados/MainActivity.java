package com.example.cruz.conactospersonalizados;


import android.app.DownloadManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView tvDatos;
    ListView lvContactos;
    Button btnActualizar, btnAgreagarnuevo;
    ImageView foto;

    static String baseUrl = "http://192.168.8.105:8080/api/values";  // Esta es la URL de la API
    // static String baseUrl="http://192.168.43.100:8080/api/values";
    ArrayList<Contactos> contactos= new ArrayList<>();
    ListaPersonalizada adaptador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvDatos = findViewById(R.id.tvDatos);
        tvDatos.setMovementMethod(new ScrollingMovementMethod());
        lvContactos = findViewById(R.id.lvContactos);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnAgreagarnuevo = findViewById(R.id.btnAgregarNuevo);


        //getList("adrianbranchbit");
        adaptador= new ListaPersonalizada(this,R.layout.listapersonalizada,contactos);
        lvContactos.setAdapter(adaptador);

        new GetContacto(baseUrl, contactos, adaptador,foto).execute();

    }
}